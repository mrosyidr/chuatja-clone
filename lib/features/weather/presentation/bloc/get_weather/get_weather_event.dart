part of 'get_weather_bloc.dart';

abstract class GetWeatherEvent extends Equatable {
  const GetWeatherEvent();

  @override
  List<Object> get props => [];
}

class GetWeatherInit extends GetWeatherEvent {}

class GetWeather extends GetWeatherEvent {
  final String searchKeyword;
  static const String defaultKeyword = 'Jakarta';

  const GetWeather({
    this.searchKeyword = defaultKeyword,
  });

  @override
  List<Object> get props => [searchKeyword];
}
